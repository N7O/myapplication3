package n7o.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Products extends Activity {

    final String TAG = "ProductsActivity";
    String user="";
    String pwd="";
    String id ="";

    ListView lists;

    JSONObject data;
    JSONArray products;
    JSONObject[] product_contents;
    Bitmap[] products_bitmaps;

    int index_get_product;
    int index_load_image;

    final int HANDLE_MSG_0_CHECK_ID = 0;
    final int HANDLE_MSG_1_PRODUCT_ID = 1;
    final int HANDLE_MSG_2_IMAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        Intent intent = getIntent();
        user = intent.getStringExtra(getString(R.string.intent_user_id));
        pwd = intent.getStringExtra(getString(R.string.intent_pwd_id));
        id = intent.getStringExtra(getString(R.string.intent_id_id));

        //Log.i("products" , "id="+id);
        lists = (ListView)findViewById(R.id.listView2);
        registerForContextMenu(lists);

        index_get_product = 0;
        index_load_image = 0;

        begin_thread_get_products();

    }

    public android.os.Handler mHandler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLE_MSG_0_CHECK_ID:
                    try {
                        products = data.getJSONArray("products");
                        if(products != null) {
                            product_contents = new JSONObject[products.length()];
                            products_bitmaps = new Bitmap[products.length()];

                            RefreshViewList();
                            start_get_detial_information();
                            start_LoadingBitmap();
                        }
                        else
                            begin_thread_get_products();

                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                    break;
                case HANDLE_MSG_1_PRODUCT_ID:
                    try {
                        if(product_contents[index_get_product] != null) {
                            RefreshViewList();
                            index_get_product++;
                        }

                        if(index_get_product < products.length())
                            start_get_detial_information();


                    }catch (Exception e)
                    {
                        Log.e(TAG, e.toString());
                    }
                    break;

                case HANDLE_MSG_2_IMAGE:
                    try {
                        if(products_bitmaps[index_load_image] != null){
                            RefreshViewList();
                            index_load_image++;
                        }
                    }catch (Exception e)
                    {
                        Log.e(TAG, e.toString());
                    }

                    Log.e(TAG, "load image");
                    if(index_load_image < products.length())
                        start_LoadingBitmap();

                    break;
            }
        }
    };

    public void enable_handle(int msg_id)
    {
        Message msg = new Message();
        msg.what = msg_id;
        mHandler.sendMessage(msg);
    }

    public void begin_thread_get_products()
    {
        Toast.makeText(getBaseContext(), getString(R.string.title_loading), Toast.LENGTH_LONG).show();
        new Thread(new Runnable() {
            public void run() {
                data = common.requestReportViaHttps2(getString(R.string.url_address_checkid) + id, user, pwd);

                enable_handle(HANDLE_MSG_0_CHECK_ID);

            }
        }).start();
    }

    public void start_get_detial_information()
    {
        new Thread(new Runnable() {
            public void run() {
                try {
                    product_contents[index_get_product] = common.requestReportViaHttps2(getString(R.string.url_address_product) + products.getJSONObject(index_get_product).getString("id"), user, pwd);
                    //count++;

                }catch (JSONException e)
                {
                    Log.e(TAG, e.toString());
                }

                enable_handle(HANDLE_MSG_1_PRODUCT_ID);
            }
        }).start();
    }

    public void start_LoadingBitmap()
    {
        new Thread(new Runnable() {
            public void run() {
                try {
                    if(!products.getJSONObject(index_load_image).isNull("imageUrl"))
                        products_bitmaps[index_load_image] = common.getBitmapFrmHttp(products.getJSONObject(index_load_image).getString("imageUrl"));
                    else
                        index_load_image++;

                }catch (JSONException e)
                {
                    Log.e(TAG, e.toString());
                }

                enable_handle(HANDLE_MSG_2_IMAGE);

            }
        }).start();
    }


    public void RefreshViewList()
    {
        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

        for (int i = 0; i < products.length(); i++) {
            HashMap<String, Object> item = new HashMap<String, Object>();
            try {
                item.put("name", products.getJSONObject(i).getString("name"));
                JSONArray autors =  products.getJSONObject(i).getJSONArray("authors");
                //Log.i("products","" + (autors.length());
                String authors_string ="";
                for(int k = 0 ; k < autors.length();k++)
                    authors_string += autors.get(k).toString()+(k !=0 ?",":"");
                item.put("author", authors_string);


                if (product_contents[i] != null)
                    if(!product_contents[i].isNull("description"))
                        item.put("description", product_contents[i].getString("description"));


                if(products_bitmaps[i] != null) {
                    item.put("pic", products_bitmaps[i]);
                }
                else
                    item.put("pic",R.mipmap.ic_launcher);

                items.add(item);
            }catch (JSONException e) {
                Log.e("products", e.toString());
            }
        }

        //lists.removeAllViewsInLayout();
        SimpleAdapter listAdapter;
        listAdapter = new SimpleAdapter(
                getBaseContext(),
                items,
                R.layout.item_layout,
                new String[]{"name", "author", "description","pic"},
                new int[]{R.id.item_name, R.id.author_name,R.id.item_description,R.id.item_picture});

        listAdapter.setViewBinder(new SimpleAdapter.ViewBinder() {

            @Override
            public boolean setViewValue(
                    View view,
                    Object data,
                    String textRepresentation) {
                // TODO Auto-generated method stub
                if((view instanceof ImageView) && (data instanceof Bitmap)) {
                    ImageView imageView = (ImageView) view;
                    Bitmap bmp = (Bitmap) data;
                    imageView.setImageBitmap(bmp);
                    return true;
                }
                return false;
            }
        });


        lists.setAdapter(listAdapter);


        lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent Infointent = new Intent(getBaseContext(), Information.class);
                Infointent.putExtra("user", user);
                Infointent.putExtra("pwd",pwd);
                try {
                    JSONObject item = products.getJSONObject(position);
                    Infointent.putExtra("id",item.getString("id"));
                }catch (Exception e)
                {
                    Log.e("test", e.toString());
                }
                startActivity(Infointent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
