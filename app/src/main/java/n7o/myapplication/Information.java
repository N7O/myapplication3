package n7o.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Information extends Activity {

    String user="";
    String pwd="";
    String id ="";
    JSONObject info;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        Intent intent = getIntent();
        user = intent.getStringExtra(getString(R.string.intent_user_id));
        pwd = intent.getStringExtra(getString(R.string.intent_pwd_id));
        id = intent.getStringExtra(getString(R.string.intent_id_id));

        new Thread(new Runnable() {
            public void run() {
                try {
                    info = common.requestReportViaHttps2(getString(R.string.url_address_product) + id, user, pwd);
                    bitmap = common.getBitmapFrmHttp(info.getString("imageUrl"));
                }catch (JSONException e)
                {
                    Log.e("info", e.toString());
                }
                Message msg = new Message();
                msg.what = 0;
                mHandler.sendMessage(msg);
            }
        }).start();

    }

    public android.os.Handler mHandler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {

            try {
                TextView name_view = (TextView)findViewById(R.id.product_name);
                name_view.setText(info.getString("name"));
                TextView name_Authors = (TextView)findViewById(R.id.product_authors);
                //name_Authors.setText(info.getString("authors"));
                JSONArray autors =  info.getJSONArray("authors");
                String authors_string ="";
                for(int k = 0 ; k < autors.length();k++)
                    authors_string += autors.get(k).toString()+(k !=0 ?",":"");
                name_Authors.setText(authors_string);
                TextView name_Description = (TextView)findViewById(R.id.product_description);
                name_Description.setText(info.getString("description"));
                ImageView img = (ImageView)findViewById(R.id.product_bitmap);
                img.setImageBitmap(bitmap);

            }catch (Exception e)
            {
                Log.e("mHandler", e.toString());
            }

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
