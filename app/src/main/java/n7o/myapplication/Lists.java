package n7o.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;


public class Lists extends Activity {

    final String TAG = "ListsActivity";
    String user="";
    String pwd="";
    JSONArray data;
    ListView lists;
    SimpleAdapter listAdapter;

    public final int HANLDE_MSG_01_GET_LISTS = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        Intent intent = getIntent();
        user = intent.getStringExtra(getString(R.string.intent_user_id));
        pwd = intent.getStringExtra(getString(R.string.intent_pwd_id));

        lists = (ListView)findViewById(R.id.listView);
        registerForContextMenu(lists);



        new Thread(new Runnable() {
            public void run() {
                data = common.requestReportViaHttps(getString(R.string.url_address_checklist),user,pwd);

                Message msg = new Message();
                msg.what = HANLDE_MSG_01_GET_LISTS;
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public android.os.Handler mHandler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANLDE_MSG_01_GET_LISTS:
                    if (data == null) {
                        Toast.makeText(getBaseContext(), getString(R.string.title_login_error), Toast.LENGTH_LONG).show();
                        Intent loginintent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(loginintent);
                        finish();
                        return;
                    }

                    List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    for (int i = 0; i < data.length(); i++) {
                        HashMap<String, Object> itemmap = new HashMap<String, Object>();

                        try {
                            JSONObject itemobj = data.getJSONObject(i);
                            itemmap.put("count", itemobj.getJSONArray("products").length());
                            itemmap.put("name", itemobj.getString("name"));
                            items.add(itemmap);

                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }


                    listAdapter = new SimpleAdapter(
                            getBaseContext(),
                            items,
                            R.layout.list_items_layout,
                            new String[]{"count", "name"},
                            new int[]{R.id.item_count, R.id.list_itemname});


                    lists.setAdapter(listAdapter);

                    lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent productintent = new Intent(getBaseContext(), Products.class);
                            productintent.putExtra(getString(R.string.intent_user_id), user);
                            productintent.putExtra(getString(R.string.intent_pwd_id), pwd);
                            try {
                                JSONObject item = data.getJSONObject(position);
                                productintent.putExtra(getString(R.string.intent_id_id), item.getString("id"));
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                            startActivity(productintent);
                        }
                    });
                    break;
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lists, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
