package n7o.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by terry on 2016/1/12.
 */
public class common {
    final static String TAG = "common";

    public static JSONArray requestReportViaHttps(String urls, String user, String pwd)
    {
        String authorizationString = "Basic " + Base64.encodeToString(
                (user + ":" + pwd).getBytes(),
                Base64.DEFAULT);
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted( final X509Certificate[] chain, final String authType ) {
                }
                @Override
                public void checkServerTrusted( final X509Certificate[] chain, final String authType ) {
                }
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init( null, trustAllCerts, new java.security.SecureRandom() );
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            HttpsURLConnection conn = null;
            URL url = new URL(urls);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty( "Content-Type", "application/json");
            conn.setRequestProperty ("Authorization", authorizationString);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(false);

            conn.setSSLSocketFactory( sslSocketFactory);

            int responseCode = conn.getResponseCode();
            if (responseCode == 200 || responseCode == 201) {
                InputStream inputStream = conn.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line);
                }
                try {
                    String json_string = builder.toString();
                    //Log.i(TAG,json_string);
                    JSONObject jsonObj = new JSONObject(json_string);
                    JSONArray report = jsonObj.getJSONArray("data");
                    //Log.i(TAG, "length = " +report.length());
                    return report;
                    //Log.i(TAG, json_string);
                }
                catch (JSONException e)
                {
                    Log.e(TAG,e.toString());
                }


            }
            else
                Log.i(TAG,"Response Error code = "+ responseCode);

        }catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return null;
    }

    public static JSONObject requestReportViaHttps2(String urls, String user, String pwd)
    {
        String authorizationString = "Basic " + Base64.encodeToString(
                (user + ":" + pwd).getBytes(),
                Base64.DEFAULT);
        try {
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted( final X509Certificate[] chain, final String authType ) {
                }
                @Override
                public void checkServerTrusted( final X509Certificate[] chain, final String authType ) {
                }
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init( null, trustAllCerts, new java.security.SecureRandom() );
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            HttpsURLConnection conn = null;
            URL url = new URL(urls);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty( "Content-Type", "application/json");
            conn.setRequestProperty ("Authorization", authorizationString);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(false);

            conn.setSSLSocketFactory( sslSocketFactory);

            int responseCode = conn.getResponseCode();
            if (responseCode == 200 || responseCode == 201) {
                InputStream inputStream = conn.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line);
                }
                try {
                    String json_string = builder.toString();
                    //Log.i(TAG,json_string);
                    JSONObject jsonObj = new JSONObject(json_string);
                    JSONObject report = jsonObj.getJSONObject("data");
                    //Log.i(TAG, "length = " +report.length());
                    return report;
                    //Log.i(TAG, json_string);
                }
                catch (JSONException e)
                {
                    Log.e(TAG,e.toString());
                }


            }
            else
                Log.i(TAG,"Response Error code = "+ responseCode);

        }catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return null;
    }

    public static Bitmap getBitmapFrmHttp(String urls)
    {
        try {
            HttpURLConnection conn = null;
            URL url = new URL(urls);
            conn = (HttpURLConnection) url.openConnection();
            //conn.setReadTimeout(10000);
            //conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            //conn.setRequestProperty( "Content-Type", "application/json");
            //conn.setRequestProperty ("Authorization", authorizationString);
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(false);

            int responseCode = conn.getResponseCode();
            if (responseCode == 200 || responseCode == 201) {
                Log.i(TAG, "image url = " + urls);
                InputStream inputStream = conn.getInputStream();
                return BitmapFactory.decodeStream(inputStream);
            }
            else
                Log.i(TAG,"Response Error code = "+ responseCode);

        }catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return null;
    }
}
